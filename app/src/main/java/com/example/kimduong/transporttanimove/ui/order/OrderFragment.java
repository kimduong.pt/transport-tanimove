package com.example.kimduong.transporttanimove.ui.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseFragment;
import com.example.kimduong.transporttanimove.ui.location.MapsActivity;
import com.example.kimduong.transporttanimove.ui.order.timeLine.TimeLineAdapter;
import com.example.kimduong.transporttanimove.ui.order.timeLine.TimeLineOrderModel;
import com.example.kimduong.transporttanimove.util.Constants;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class OrderFragment extends BaseFragment implements OrderView, TimeLineAdapter.OnClickTimeLine{


    @BindView(R.id.rv_timeline)
    RecyclerView recycleViewTimeLine;
    @BindView(R.id.btn_motorbike)
    ImageView btnMotorbike;
    @BindView(R.id.btn_truck)
    ImageView btnTruck;
    @BindView(R.id.btn_container)
    ImageView btnContainer;
    private final float SELECTOR = 1;

    public final static  int INSERT_DONHANG = 111;
    public final static  int CHITIET_DONHANG = 112;


    List<ImageView> btnVehicleList;
    @BindView(R.id.tv_motorbike)
    TextView tvMotorbike;
    @BindView(R.id.tv_truck)
    TextView tvTruck;
    @BindView(R.id.tv_container)
    TextView tvContainer;
    private List<TextView> textViewList;
    private Context context;

    public OrderFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.context = container.getContext();
        ;
        View view = inflater.inflate(R.layout.order_fragment, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        setUp(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void setUp(View view) {
        btnVehicleList = new ArrayList<>();
        btnVehicleList.add(btnContainer);
        btnVehicleList.add(btnMotorbike);
        btnVehicleList.add(btnTruck);
        textViewList = new ArrayList<>();
        textViewList.add(tvContainer);
        textViewList.add(tvTruck);
        textViewList.add(tvMotorbike);
        initTimeline();


    }

    private void initTimeline() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), RecyclerView.VERTICAL, false);
        recycleViewTimeLine.setLayoutManager(layoutManager);
        TimeLineAdapter timeLineAdapter = new TimeLineAdapter();
        timeLineAdapter.setOnClickTimeLine(this);
        List<TimeLineOrderModel> modelList = new ArrayList<>();
        modelList.add(new TimeLineOrderModel("Header Here"));
        timeLineAdapter.setmList(modelList);
        recycleViewTimeLine.setAdapter(timeLineAdapter);
    }

    private void clearSelectVehicle() {
   /*     for (ImageView imageView : btnVehicleList) {
            switch (imageView.getId()){
                case R.id.btn_container:
                    changeIconColor(imageView,R.drawable.ic_container,R.color.colorTransparent);
                    break;
                case R.id.btn_motorbike:
                    changeIconColor(imageView,R.drawable.ic_motor,R.color.colorTransparent);
                    break;
                case R.id.btn_truck:
                    changeIconColor(imageView,R.drawable.ic_truck,R.color.colorTransparent);
                    break;
                    default:break;
            }
            imageView.setAlpha(0.3f);
        }*/
        for (int index = 0; index < 3; index ++){
            textViewList.get(index).setTextColor(getResources().getColor(R.color.colorShadow));
            btnVehicleList.get(index).setAlpha(0.3f);
        }

    }
//    private void setSelector(ImageView view){
//        clearSelectVehicle();
////        view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.rv_timeline)
    public void onViewClicked() {
    }

    @OnClick({R.id.btn_motorbike, R.id.btn_truck, R.id.btn_container})
    public void onViewClicked(View view) {
        clearSelectVehicle();
        view.setAlpha(SELECTOR);
        switch (view.getId()) {
            case R.id.btn_container:
                tvContainer.setTextColor(getResources().getColor(R.color.colorPrimary));
//                changeIconColor(btnContainer,R.drawable.ic_container,R.color.colorPrimary);
                break;
            case R.id.btn_motorbike:
                tvMotorbike.setTextColor(getResources().getColor(R.color.colorPrimary));
//                changeIconColor(btnMotorbike,R.drawable.ic_motor,R.color.colorPrimary);
                break;
            case R.id.btn_truck:
                tvTruck.setTextColor(getResources().getColor(R.color.colorPrimary));
//                changeIconColor(btnTruck,R.drawable.ic_truck,R.color.colorPrimary);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClickStartOrder() {
        Timber.e("onClickStartOrder");
        Intent intent = new Intent(getBaseActivity(), MapsActivity.class);
        intent.putExtra(Constants.MAP_VIEW_MODE,INSERT_DONHANG);
        startActivity(intent);
    }

    @Override
    public void onClickEditOrder() {
        Timber.e("onClickEditOrder");
        Intent intent = new Intent(getBaseActivity(), MapsActivity.class);
        intent.putExtra(Constants.MAP_VIEW_MODE,CHITIET_DONHANG);
        startActivity(intent);
    }

    @Override
    public void onClickAddOrder() {
        Timber.e("onClickAddOrder");
        Intent intent = new Intent(getBaseActivity(), MapsActivity.class);
        intent.putExtra(Constants.MAP_VIEW_MODE,CHITIET_DONHANG);
        startActivity(intent);

    }

   /* public void changeIconColor(ImageView imageView, int drawbleIcon, int color) {
        Drawable mIcon= ContextCompat.getDrawable(context, drawbleIcon);
        if (mIcon != null) {
            mIcon.setColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.MULTIPLY);
        }
        imageView.setImageDrawable(mIcon);

    }*/
}
