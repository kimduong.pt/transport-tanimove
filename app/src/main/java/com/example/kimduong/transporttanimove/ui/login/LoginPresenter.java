package com.example.kimduong.transporttanimove.ui.login;

import com.example.kimduong.transporttanimove.base.BaseMvpPresenter;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetIDKhachHangRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.LoginRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.service.SOAPServiceAuth;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class LoginPresenter extends BaseMvpPresenter<LoginView> {
    private SOAPServiceAuth soapServiceAuth = SOAPServiceAuth.getInstance();

    public LoginPresenter() {

    }

    public void signIn(String username, String password) {

        getCompositeDisposable().add(soapServiceAuth.logInUserNamePassword(new LoginRequest(username, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(string -> {
                    if(string.equals("1")){
                        //Login Successfully
                        getIdKhachHang(username);
                    }else {
                        getMvpView().showSimpleDialog("Lỗi",string,null);
                    }
                },this::handleError)
        );
    }

    private void getIdKhachHang(String username){
        Disposable disposable = soapServiceAuth.getIdKhachHang(new GetIDKhachHangRequest(username))
                .subscribe(integer -> {
                    Timber.d("Savingx ID Customer");
                    getMvpView().saveIDSharePre(integer);
                    Timber.d("Saved ID Customer");
                },this::handleError);
        getCompositeDisposable().add(disposable);

    }



}
