package com.example.kimduong.transporttanimove.model;

import com.example.kimduong.transporttanimove.serviceSOAP.request.InsertChiTietDonHangRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.InsertDonHangRequest;

import java.util.List;

public class ManagementOrder  {
    private InsertDonHangRequest insertDonHangRequest;

    private List<InsertChiTietDonHangRequest> listChiTietDonHang;

    private static ManagementOrder INSTANCE;

    private ManagementOrder(){

    }

    public static ManagementOrder getINSTANCE() {
        if(INSTANCE == null){
            INSTANCE = new ManagementOrder();
        }
        return INSTANCE;
    }

    public InsertDonHangRequest getInsertDonHangRequest() {
        return insertDonHangRequest;
    }

    public List<InsertChiTietDonHangRequest> getListChiTietDonHang() {
        return listChiTietDonHang;
    }

    public void setInsertDonHangRequest(InsertDonHangRequest insertDonHangRequest) {
        this.insertDonHangRequest = insertDonHangRequest;
    }

    public static void clearALL(){
        INSTANCE = null;
    }
}
