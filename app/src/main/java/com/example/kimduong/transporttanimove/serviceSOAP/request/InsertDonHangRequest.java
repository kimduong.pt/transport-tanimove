package com.example.kimduong.transporttanimove.serviceSOAP.request;


import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

/**
 * {@link com.example.kimduong.transporttanimove.R.xml#insert_don_hang}
 *
* */


@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class InsertDonHangRequest extends SOAPSerializable {

    public InsertDonHangRequest() {
    }

    @JSoapReqField(order = 0,fieldName = "IDKhachHang")
    int IDKhachHang;

    @JSoapReqField(order = 0,fieldName = "maTinhNhanHang")
    int maTinhNhanHang;

    @JSoapReqField(order = 0,fieldName = "maHuyenNhanHang")
    int maHuyenNhanHang;

    @JSoapReqField(order = 0,fieldName = "maXaPhuongNhanHang")
    int maXaPhuongNhanHang;

    @JSoapReqField(order = 0,fieldName = "diaChiNhanHang")
    String diaChiNhanHang;

    @JSoapReqField(order = 0,fieldName = "toaDoNhanHang")
    String toaDoNhanHang;

    @JSoapReqField(order = 0,fieldName = "thongTinLienHe")
    String thongTinLienHe;

    @JSoapReqField(order = 0,fieldName = "noiDungVanChuyen")
    String noiDungVanChuyen;

    @JSoapReqField(order = 0,fieldName = "thuHo")
    Boolean thuHo;

    @JSoapReqField(order = 0,fieldName = "giaoTanTay")
    Boolean giaoTanTay;

    public int getIDKhachHang() {
        return IDKhachHang;
    }

    public void setIDKhachHang(int IDKhachHang) {
        this.IDKhachHang = IDKhachHang;
    }

    public int getMaTinhNhanHang() {
        return maTinhNhanHang;
    }

    public void setMaTinhNhanHang(int maTinhNhanHang) {
        this.maTinhNhanHang = maTinhNhanHang;
    }

    public int getMaHuyenNhanHang() {
        return maHuyenNhanHang;
    }

    public void setMaHuyenNhanHang(int maHuyenNhanHang) {
        this.maHuyenNhanHang = maHuyenNhanHang;
    }

    public int getMaXaPhuongNhanHang() {
        return maXaPhuongNhanHang;
    }

    public void setMaXaPhuongNhanHang(int maXaPhuongNhanHang) {
        this.maXaPhuongNhanHang = maXaPhuongNhanHang;
    }

    public String getDiaChiNhanHang() {
        return diaChiNhanHang;
    }

    public void setDiaChiNhanHang(String diaChiNhanHang) {
        this.diaChiNhanHang = diaChiNhanHang;
    }

    public String getToaDoNhanHang() {
        return toaDoNhanHang;
    }

    public void setToaDoNhanHang(String toaDoNhanHang) {
        this.toaDoNhanHang = toaDoNhanHang;
    }

    public String getThongTinLienHe() {
        return thongTinLienHe;
    }

    public void setThongTinLienHe(String thongTinLienHe) {
        this.thongTinLienHe = thongTinLienHe;
    }

    public String getNoiDungVanChuyen() {
        return noiDungVanChuyen;
    }

    public void setNoiDungVanChuyen(String noiDungVanChuyen) {
        this.noiDungVanChuyen = noiDungVanChuyen;
    }

    public Boolean getThuHo() {
        return thuHo;
    }

    public void setThuHo(Boolean thuHo) {
        this.thuHo = thuHo;
    }

    public Boolean getGiaoTanTay() {
        return giaoTanTay;
    }

    public void setGiaoTanTay(Boolean giaoTanTay) {
        this.giaoTanTay = giaoTanTay;
    }

    @Override
    public String toString() {
        return "InsertDonHangRequest{" +
                "IDKhachHang=" + IDKhachHang +
                ", maTinhNhanHang=" + maTinhNhanHang +
                ", maHuyenNhanHang=" + maHuyenNhanHang +
                ", maXaPhuongNhanHang=" + maXaPhuongNhanHang +
                ", diaChiNhanHang='" + diaChiNhanHang + '\'' +
                ", toaDoNhanHang='" + toaDoNhanHang + '\'' +
                ", thongTinLienHe='" + thongTinLienHe + '\'' +
                ", noiDungVanChuyen='" + noiDungVanChuyen + '\'' +
                ", thuHo=" + thuHo +
                ", giaoTanTay=" + giaoTanTay +
                '}';
    }


}
