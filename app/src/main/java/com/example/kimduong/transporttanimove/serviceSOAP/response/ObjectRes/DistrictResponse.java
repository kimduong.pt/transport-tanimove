package com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class DistrictResponse extends SOAPDeserializable {
    @JSoapResField(name = "ID")
    public Integer id;
    @JSoapResField(name = "tenHuyen")
    public String districtName;

    @JSoapResField(name = "maTinh")
    public Integer idCity;

    public Integer getId() {
        return id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public Integer getCityId() {
        return idCity;
    }

    @Override
    public String toString() {
        return String.format("ID : %d - cityName: %s \n ",id,districtName );
    }
}
