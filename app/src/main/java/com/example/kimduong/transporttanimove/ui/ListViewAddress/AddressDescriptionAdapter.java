package com.example.kimduong.transporttanimove.ui.ListViewAddress;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.model.AddressDescription;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressDescriptionAdapter extends RecyclerView.Adapter<AddressDescriptionAdapter.ViewHolder> {
    private List<AddressDescription> addressObjects;
    private ViewHolder viewHolder;
    private OnItemListener onItemListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View items = layoutInflater.inflate(R.layout.row_item_simple_list_address, parent, false);
        this.viewHolder = new ViewHolder(items);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(addressObjects.get(position).getName());
    }

    public AddressDescriptionAdapter(List<AddressDescription> addressObjects){
        this.addressObjects = addressObjects;
    }

    @Override
    public int getItemCount() {
        return addressObjects != null? addressObjects.size() : 0;
    }

    public void setAddressObject(List<AddressDescription> addressDescriptions) {
        this.addressObjects = addressDescriptions;
        notifyDataSetChanged();
    }

    public void setOnItemListener(OnItemListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_row_name)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.tv_row_name)
        void onClickItemAddress() {
            onItemListener.getAddressFromList(addressObjects.get(getPosition()));
        }
    }

    interface OnItemListener{
        void getAddressFromList(AddressDescription addressDescription);
    }
}


