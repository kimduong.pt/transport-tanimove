package com.example.kimduong.transporttanimove.ui.register;

import com.example.kimduong.transporttanimove.base.MvpView;

public interface RegisterView extends MvpView {
    void finishActivity();
}
