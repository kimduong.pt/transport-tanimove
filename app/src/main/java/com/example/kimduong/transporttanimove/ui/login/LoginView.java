package com.example.kimduong.transporttanimove.ui.login;

import com.example.kimduong.transporttanimove.base.MvpView;

public interface LoginView extends MvpView {
    void goToMainActivity();

    void saveIDSharePre(int id);
}
