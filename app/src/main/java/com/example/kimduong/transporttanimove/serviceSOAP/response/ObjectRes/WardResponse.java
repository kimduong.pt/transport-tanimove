package com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class WardResponse extends SOAPDeserializable {
    @JSoapResField(name = "ID")
    public Integer id;
    @JSoapResField(name = "tenXaPhuong")
    public String wardName  ;

    @JSoapResField(name = "maHuyen")
    public String idCity;
    @JSoapResField(name = "maTinh")
    public String idProvince  ;

    public Integer getId() {
        return id;
    }

    public String getWardName() {
        return wardName;
    }

    public String getIdCity() {
        return idCity;
    }

    public String getIdProvince() {
        return idProvince;
    }

    @Override
    public String toString() {
        return String.format("ID : %d - wardName: %s \n ",id,wardName );
    }
}
