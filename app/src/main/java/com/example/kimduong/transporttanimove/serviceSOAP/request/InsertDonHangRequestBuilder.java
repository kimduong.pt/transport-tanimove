package com.example.kimduong.transporttanimove.serviceSOAP.request;

public final class InsertDonHangRequestBuilder {
    int IDKhachHang;
    int maTinhNhanHang;
    int maHuyenNhanHang;
    int maXaPhuongNhanHang;
    String diaChiNhanHang;
    String toaDoNhanHang;
    String thongTinLienHe;
    String noiDungVanChuyen;
    Boolean thuHo;
    Boolean giaoTanTay;

    private InsertDonHangRequestBuilder() {
    }

    public static InsertDonHangRequestBuilder anInsertDonHangRequest() {
        return new InsertDonHangRequestBuilder();
    }

    public InsertDonHangRequestBuilder withIDKhachHang(int IDKhachHang) {
        this.IDKhachHang = IDKhachHang;
        return this;
    }

    public InsertDonHangRequestBuilder withMaTinhNhanHang(int maTinhNhanHang) {
        this.maTinhNhanHang = maTinhNhanHang;
        return this;
    }

    public InsertDonHangRequestBuilder withMaHuyenNhanHang(int maHuyenNhanHang) {
        this.maHuyenNhanHang = maHuyenNhanHang;
        return this;
    }

    public InsertDonHangRequestBuilder withMaXaPhuongNhanHang(int maXaPhuongNhanHang) {
        this.maXaPhuongNhanHang = maXaPhuongNhanHang;
        return this;
    }

    public InsertDonHangRequestBuilder withDiaChiNhanHang(String diaChiNhanHang) {
        this.diaChiNhanHang = diaChiNhanHang;
        return this;
    }

    public InsertDonHangRequestBuilder withToaDoNhanHang(String toaDoNhanHang) {
        this.toaDoNhanHang = toaDoNhanHang;
        return this;
    }

    public InsertDonHangRequestBuilder withThongTinLienHe(String thongTinLienHe) {
        this.thongTinLienHe = thongTinLienHe;
        return this;
    }

    public InsertDonHangRequestBuilder withNoiDungVanChuyen(String noiDungVanChuyen) {
        this.noiDungVanChuyen = noiDungVanChuyen;
        return this;
    }

    public InsertDonHangRequestBuilder withThuHo(Boolean thuHo) {
        this.thuHo = thuHo;
        return this;
    }

    public InsertDonHangRequestBuilder withGiaoTanTay(Boolean giaoTanTay) {
        this.giaoTanTay = giaoTanTay;
        return this;
    }

    public InsertDonHangRequest build() {
        InsertDonHangRequest insertDonHangRequest = new InsertDonHangRequest();
        insertDonHangRequest.setIDKhachHang(IDKhachHang);
        insertDonHangRequest.setMaTinhNhanHang(maTinhNhanHang);
        insertDonHangRequest.setMaHuyenNhanHang(maHuyenNhanHang);
        insertDonHangRequest.setMaXaPhuongNhanHang(maXaPhuongNhanHang);
        insertDonHangRequest.setDiaChiNhanHang(diaChiNhanHang);
        insertDonHangRequest.setToaDoNhanHang(toaDoNhanHang);
        insertDonHangRequest.setThongTinLienHe(thongTinLienHe);
        insertDonHangRequest.setNoiDungVanChuyen(noiDungVanChuyen);
        insertDonHangRequest.setThuHo(thuHo);
        insertDonHangRequest.setGiaoTanTay(giaoTanTay);
        return insertDonHangRequest;
    }
}
