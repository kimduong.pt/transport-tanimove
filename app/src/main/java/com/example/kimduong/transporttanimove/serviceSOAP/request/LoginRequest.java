package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class LoginRequest extends SOAPSerializable {
    @JSoapReqField(order = 0,fieldName = "tenDangNhap")
    String username;
    @JSoapReqField(order = 0,fieldName = "matKhau")
    String password;


    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
