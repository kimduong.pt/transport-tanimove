package com.example.kimduong.transporttanimove.serviceSOAP.request;

public final class InsertChiTietDonHangRequestBuilder {
    private Integer iDDonHang;
    private Integer maPhuongTienVanChuyen;
    private Integer maMaLoaiHang;
    private Integer maTinh;
    private Integer maHuyen;
    private Integer maXaPhuong;
    private String diaChiDiemGiao;
    private String toaDoDiemGiao;
    private Integer khoangCach;
    private Integer ktChieuCao;
    private Integer ktChieuDai;
    private Integer ktChieuRong;
    private Integer canNang;
    private Integer soLuong;
    private String ghiChu;
    private String lienHe;

    private InsertChiTietDonHangRequestBuilder() {
    }

    public static InsertChiTietDonHangRequestBuilder anInsertChiTietDonHangRequest() {
        return new InsertChiTietDonHangRequestBuilder();
    }

    public InsertChiTietDonHangRequestBuilder withIDDonHang(Integer iDDonHang) {
        this.iDDonHang = iDDonHang;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withMaPhuongTienVanChuyen(Integer maPhuongTienVanChuyen) {
        this.maPhuongTienVanChuyen = maPhuongTienVanChuyen;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withMaMaLoaiHang(Integer maMaLoaiHang) {
        this.maMaLoaiHang = maMaLoaiHang;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withMaTinh(Integer maTinh) {
        this.maTinh = maTinh;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withMaHuyen(Integer maHuyen) {
        this.maHuyen = maHuyen;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withMaXaPhuong(Integer maXaPhuong) {
        this.maXaPhuong = maXaPhuong;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withDiaChiDiemGiao(String diaChiDiemGiao) {
        this.diaChiDiemGiao = diaChiDiemGiao;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withToaDoDiemGiao(String toaDoDiemGiao) {
        this.toaDoDiemGiao = toaDoDiemGiao;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withKhoangCach(Integer khoangCach) {
        this.khoangCach = khoangCach;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withKtChieuCao(Integer ktChieuCao) {
        this.ktChieuCao = ktChieuCao;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withKtChieuDai(Integer ktChieuDai) {
        this.ktChieuDai = ktChieuDai;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withKtChieuRong(Integer ktChieuRong) {
        this.ktChieuRong = ktChieuRong;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withCanNang(Integer canNang) {
        this.canNang = canNang;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public InsertChiTietDonHangRequestBuilder withLienHe(String lienHe) {
        this.lienHe = lienHe;
        return this;
    }

    public InsertChiTietDonHangRequest build() {
        InsertChiTietDonHangRequest insertChiTietDonHangRequest = new InsertChiTietDonHangRequest();
        insertChiTietDonHangRequest.setIDDonHang(iDDonHang);
        insertChiTietDonHangRequest.setMaPhuongTienVanChuyen(maPhuongTienVanChuyen);
        insertChiTietDonHangRequest.setMaMaLoaiHang(maMaLoaiHang);
        insertChiTietDonHangRequest.setMaTinh(maTinh);
        insertChiTietDonHangRequest.setMaHuyen(maHuyen);
        insertChiTietDonHangRequest.setMaXaPhuong(maXaPhuong);
        insertChiTietDonHangRequest.setDiaChiDiemGiao(diaChiDiemGiao);
        insertChiTietDonHangRequest.setToaDoDiemGiao(toaDoDiemGiao);
        insertChiTietDonHangRequest.setKhoangCach(khoangCach);
        insertChiTietDonHangRequest.setKtChieuCao(ktChieuCao);
        insertChiTietDonHangRequest.setKtChieuDai(ktChieuDai);
        insertChiTietDonHangRequest.setKtChieuRong(ktChieuRong);
        insertChiTietDonHangRequest.setCanNang(canNang);
        insertChiTietDonHangRequest.setSoLuong(soLuong);
        insertChiTietDonHangRequest.setGhiChu(ghiChu);
        insertChiTietDonHangRequest.setLienHe(lienHe);
        return insertChiTietDonHangRequest;
    }
}
