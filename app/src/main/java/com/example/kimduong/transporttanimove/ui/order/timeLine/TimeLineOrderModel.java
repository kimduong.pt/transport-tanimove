package com.example.kimduong.transporttanimove.ui.order.timeLine;

public class TimeLineOrderModel {
    private String content;

    public TimeLineOrderModel(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



}
