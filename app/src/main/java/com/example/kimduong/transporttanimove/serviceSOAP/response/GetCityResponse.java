package com.example.kimduong.transporttanimove.serviceSOAP.response;

import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.CityResponse;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class GetCityResponse extends SOAPDeserializable {
    @JSoapResField(name = "tinh")
    public CityResponse[] provinceRes;

    public CityResponse[] getProvinceRes() {
        return provinceRes;
    }
}
