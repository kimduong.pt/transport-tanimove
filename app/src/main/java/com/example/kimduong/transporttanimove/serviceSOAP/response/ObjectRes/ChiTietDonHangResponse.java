package com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class ChiTietDonHangResponse extends SOAPDeserializable {

    @JSoapResField(name ="ID")

    private Integer iD;
    @JSoapResField(name ="IDDonHang")

    private Integer iDDonHang;
    @JSoapResField(name ="maPhuongTienVanChuyen")

    private Integer maPhuongTienVanChuyen;
    @JSoapResField(name ="maMaLoaiHang")

    private Integer maMaLoaiHang;
    @JSoapResField(name ="maTinh")

    private Integer maTinh;
    @JSoapResField(name ="maHuyen")

    private Integer maHuyen;
    @JSoapResField(name ="maXaPhuong")

    private Integer maXaPhuong;
    @JSoapResField(name ="diaChiDiemGiao")

    private String diaChiDiemGiao;
    @JSoapResField(name ="toaDoDiemGiao")

    private String toaDoDiemGiao;
    @JSoapResField(name ="khoangCach")

    private Integer khoangCach;
    @JSoapResField(name ="ktChieuCao")

    private Integer ktChieuCao;
    @JSoapResField(name ="ktChieuDai")

    private Integer ktChieuDai;
    @JSoapResField(name ="ktChieuRong")

    private Integer ktChieuRong;
    @JSoapResField(name ="canNang")

    private String canNang;
    @JSoapResField(name ="noiDung")

    private String noiDung;
    @JSoapResField(name ="lienHe")

    private String lienHe;
    @JSoapResField(name ="IDNguoiGiao")

    private Integer iDNguoiGiao;
    @JSoapResField(name ="tenNguoiGiao")

    private String tenNguoiGiao;
    @JSoapResField(name ="thoiGianGiao")

    private String thoiGianGiao;
    @JSoapResField(name ="tienVanChuyen")

    private String tienVanChuyen;
    @JSoapResField(name ="phiPhatSinhCong")

    private String phiPhatSinhCong;

    @JSoapResField(name ="phiPhatSinhTru")
    private String phiPhatSinhTru;

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public Integer getiDDonHang() {
        return iDDonHang;
    }

    public void setiDDonHang(Integer iDDonHang) {
        this.iDDonHang = iDDonHang;
    }

    public Integer getMaPhuongTienVanChuyen() {
        return maPhuongTienVanChuyen;
    }

    public void setMaPhuongTienVanChuyen(Integer maPhuongTienVanChuyen) {
        this.maPhuongTienVanChuyen = maPhuongTienVanChuyen;
    }

    public Integer getMaMaLoaiHang() {
        return maMaLoaiHang;
    }

    public void setMaMaLoaiHang(Integer maMaLoaiHang) {
        this.maMaLoaiHang = maMaLoaiHang;
    }

    public Integer getMaTinh() {
        return maTinh;
    }

    public void setMaTinh(Integer maTinh) {
        this.maTinh = maTinh;
    }

    public Integer getMaHuyen() {
        return maHuyen;
    }

    public void setMaHuyen(Integer maHuyen) {
        this.maHuyen = maHuyen;
    }

    public Integer getMaXaPhuong() {
        return maXaPhuong;
    }

    public void setMaXaPhuong(Integer maXaPhuong) {
        this.maXaPhuong = maXaPhuong;
    }

    public String getDiaChiDiemGiao() {
        return diaChiDiemGiao;
    }

    public void setDiaChiDiemGiao(String diaChiDiemGiao) {
        this.diaChiDiemGiao = diaChiDiemGiao;
    }

    public String getToaDoDiemGiao() {
        return toaDoDiemGiao;
    }

    public void setToaDoDiemGiao(String toaDoDiemGiao) {
        this.toaDoDiemGiao = toaDoDiemGiao;
    }

    public Integer getKhoangCach() {
        return khoangCach;
    }

    public void setKhoangCach(Integer khoangCach) {
        this.khoangCach = khoangCach;
    }

    public Integer getKtChieuCao() {
        return ktChieuCao;
    }

    public void setKtChieuCao(Integer ktChieuCao) {
        this.ktChieuCao = ktChieuCao;
    }

    public Integer getKtChieuDai() {
        return ktChieuDai;
    }

    public void setKtChieuDai(Integer ktChieuDai) {
        this.ktChieuDai = ktChieuDai;
    }

    public Integer getKtChieuRong() {
        return ktChieuRong;
    }

    public void setKtChieuRong(Integer ktChieuRong) {
        this.ktChieuRong = ktChieuRong;
    }

    public String getCanNang() {
        return canNang;
    }

    public void setCanNang(String canNang) {
        this.canNang = canNang;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getLienHe() {
        return lienHe;
    }

    public void setLienHe(String lienHe) {
        this.lienHe = lienHe;
    }

    public Integer getiDNguoiGiao() {
        return iDNguoiGiao;
    }

    public void setiDNguoiGiao(Integer iDNguoiGiao) {
        this.iDNguoiGiao = iDNguoiGiao;
    }

    public String getTenNguoiGiao() {
        return tenNguoiGiao;
    }

    public void setTenNguoiGiao(String tenNguoiGiao) {
        this.tenNguoiGiao = tenNguoiGiao;
    }

    public String getThoiGianGiao() {
        return thoiGianGiao;
    }

    public void setThoiGianGiao(String thoiGianGiao) {
        this.thoiGianGiao = thoiGianGiao;
    }

    public String getTienVanChuyen() {
        return tienVanChuyen;
    }

    public void setTienVanChuyen(String tienVanChuyen) {
        this.tienVanChuyen = tienVanChuyen;
    }

    public String getPhiPhatSinhCong() {
        return phiPhatSinhCong;
    }

    public void setPhiPhatSinhCong(String phiPhatSinhCong) {
        this.phiPhatSinhCong = phiPhatSinhCong;
    }

    public String getPhiPhatSinhTru() {
        return phiPhatSinhTru;
    }

    public void setPhiPhatSinhTru(String phiPhatSinhTru) {
        this.phiPhatSinhTru = phiPhatSinhTru;
    }


}
