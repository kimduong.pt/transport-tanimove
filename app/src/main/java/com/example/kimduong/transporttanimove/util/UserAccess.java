package com.example.kimduong.transporttanimove.util;

public class UserAccess {
    private static UserAccess userAccess;
    private int ID_USER;
    private UserAccess() {
        //no instance
    }

    public static UserAccess getInstance() {
        if(userAccess == null){
            userAccess = new UserAccess();
        }
        return userAccess;
    }

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }
}
