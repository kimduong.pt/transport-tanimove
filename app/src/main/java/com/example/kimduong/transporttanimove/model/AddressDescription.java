package com.example.kimduong.transporttanimove.model;

public class AddressDescription {
    private int id;
    private String name;
    private String field;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public AddressDescription(int id, String name, String field) {
        this.id = id;
        this.name = name;
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
