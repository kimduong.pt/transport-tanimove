package com.example.kimduong.transporttanimove.serviceSOAP.service;

import com.example.kimduong.transporttanimove.serviceSOAP.request.EmtryRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetDistrictRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetIDKhachHangRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetWardRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.LoginRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.RegisterRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.UpdateProfileRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.response.GetCityResponse;
import com.example.kimduong.transporttanimove.serviceSOAP.response.GetDistrictResponse;
import com.example.kimduong.transporttanimove.serviceSOAP.response.GetWardResponse;

import io.reactivex.Observable;
import pt.joaocruz04.lib.SOAPManager;
import pt.joaocruz04.lib.misc.JSoapCallback;
import timber.log.Timber;

public class SOAPServiceAuth extends SoapHelper {
    private static SOAPServiceAuth soapServiceAuth;

    public static SOAPServiceAuth getInstance() {
        if(soapServiceAuth ==null){
            soapServiceAuth = new SOAPServiceAuth(); }
        return soapServiceAuth;
    }

    public  Observable<String> logInUserNamePassword(LoginRequest loginRequest) {
        String action = "http://tanimove.com/LogIn";
        String methodName = "LogIn";
        return Observable.create(emitter -> {
            SOAPManager.get(NAMESPACE, URL, methodName, action , loginRequest, String.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object result) {
                    emitter.onNext(String.valueOf(result));
                }

                @Override
                public void onError(int error) {
                    emitter.onError(new Throwable());
                }

                @Override
                public void onDebugMessage(String title, String message) {
                    super.onDebugMessage(title, message);
                    Timber.d("Title : %s, Message : %s", title, message);
                }
            });
        });
    }



    public  Observable<GetCityResponse> getListCity() {
        String action = "http://tanimove.com/ListTinh";
        String method = "ListTinh";

        return Observable.create(emitter -> {
            SOAPManager.get(NAMESPACE, URL, method, action, new EmtryRequest(), GetCityResponse.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object result) {
                    emitter.onNext((GetCityResponse) result);
                }

                @Override
                public void onError(int error) {
                    emitter.onError(new Throwable());

                }
            });
        });
    }

    public  Observable<GetDistrictResponse> getDistrictResponseObservable(GetDistrictRequest getDistrictRequest) {
        String method = "ListHuyenTheoTinh";
        return Observable.create(emitter -> {
            SOAPManager.get(NAMESPACE, URL, method, NAMESPACE + method, getDistrictRequest, GetDistrictResponse.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object result) {
                    emitter.onNext((GetDistrictResponse) result);
                }

                @Override
                public void onError(int error) {
                    emitter.onError(new Throwable());

                }

                @Override
                public void onDebugMessage(String title, String message) {
                    Timber.d("Title : %s, Message : %s", title, message);
                    super.onDebugMessage(title, message);
                }
            });
        });
    }

    public  Observable<GetWardResponse> getWardResponseObservable(GetWardRequest getWardRequest) {
        String method = "ListXaPhuongTheoHuyen";
        return Observable.create(emitter ->
                SOAPManager.get(NAMESPACE, URL, method, NAMESPACE + method, getWardRequest, GetWardResponse.class, new JSoapCallback() {
                    @Override
                    public void onSuccess(Object result) {
                        emitter.onNext((GetWardResponse) result);
                    }

                    @Override
                    public void onError(int error) {
                        emitter.onError(new Throwable());
                        Timber.e(String.valueOf(error));

                    }

                    @Override
                    public void onDebugMessage(String title, String message) {
                        Timber.d(String.format("Title : %s, Message : %s", title, message));
                        super.onDebugMessage(title, message);
                    }
                }));
    }

    public Observable<Integer> createAndUpdateProfile(UpdateProfileRequest updateProfileRequest){
        String method = "UpdateThongTinKhachHang";
        return Observable.create(emitter -> {
            SOAPManager.get(NAMESPACE, URL, method, NAMESPACE + method, updateProfileRequest, Integer.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object result) {
                    emitter.onNext((int)result);
                }

                @Override
                public void onError(int error) {
                    emitter.onError(new Throwable());
                }
            });
        });
    }

    public Observable<String> register(RegisterRequest registerRequest){
        String method = "InsertKhachHang";
        return Observable.create(emitter -> {
            SOAPManager.get(NAMESPACE, URL, method, NAMESPACE + method, registerRequest, String.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object result) {
                    emitter.onNext(String.valueOf(result));
                }

                @Override
                public void onError(int error) {
                    emitter.onError(new Throwable());
                }

                @Override
                public void onDebugMessage(String title, String message) {
                    super.onDebugMessage(title, message);
                    Timber.d("Title : %s, Message : %s", title, message);
                }
            });
        });
    }

    public Observable<Integer> getIdKhachHang(GetIDKhachHangRequest getIDKhachHangRequest){
        String action = "GetIDKhachHang";
        return Observable.create(emitter ->{
            SOAPManager.get(NAMESPACE, URL, action, getAction(action), getIDKhachHangRequest, Integer.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object o) {
                    int id = (int) o;
                    Timber.e(action + id);
                    emitter.onNext(id);
                }

                @Override
                public void onError(int i) {
                    emitter.onError(new Throwable());
                }

                @Override
                public void onDebugMessage(String title, String message) {
                    super.onDebugMessage(title, message);
                    Timber.d("Title : %s, Message : %s", title, message);

                }
            });
        });
    }


}
