package com.example.kimduong.transporttanimove.model.eventBus;

public class AddressEvents {
    private int idCity;
    private String nameCity;
    private int idDistrict;
    private String nameDistrict;
    private int idWard;
    private String nameWard;

    public int getIdCity() {
        return idCity;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public int getIdWard() {
        return idWard;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    public String getNameDistrict() {
        return nameDistrict;
    }

    public void setNameDistrict(String nameDistrict) {
        this.nameDistrict = nameDistrict;
    }

    public String getNameWard() {
        return nameWard;
    }

    public void setNameWard(String nameWard) {
        this.nameWard = nameWard;
    }

    public void setIdCity(int idCity) {
        this.idCity = idCity;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public void setIdWard(int idWard) {
        this.idWard = idWard;
    }
}
