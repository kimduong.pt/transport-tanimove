package com.example.kimduong.transporttanimove;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.example.kimduong.transporttanimove.ui.intro.IntroActivity;
import com.example.kimduong.transporttanimove.ui.order.OrderFragment;
import com.example.kimduong.transporttanimove.util.Constants;
import com.example.kimduong.transporttanimove.util.SharedPreferences;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.viven.fragmentstatemanager.FragmentStateManager;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FragmentStateManager fragmentStateManager;
    private final static int LIST_ORDER_FRAGMENT = 1;
    private final static int ORDER_FRAGMENT = 0;
    private final static int ALERT_FRAGMENT = 2;
    private final static int HELP_FRAGMENT = 3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        checkLoginUser();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupNavigation();
        setupFragStateManager();
        setUp();
    }

    private void checkLoginUser() {
        SharedPreferences instance = SharedPreferences.getInstance(this);
        int idUser = instance.getInteger(Constants.ID_CUSTOMER,0);
        if(idUser == 0){
            goToIntroScreen();
        }
    }

    private void goToIntroScreen() {
        Intent intent = new Intent(this, IntroActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void setUp() {
//        new TestServiceSOAP();

    }

    private void setupFragStateManager() {
        FrameLayout content = findViewById(R.id.container_fragment);
        fragmentStateManager = new FragmentStateManager(content, getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case ORDER_FRAGMENT: {
                        return new OrderFragment();
                    }
                    case LIST_ORDER_FRAGMENT:
//                        return new OrderListFragment();

                    case ALERT_FRAGMENT:
//                        return new MeFragment();

                    case HELP_FRAGMENT:{

                    }
                    default: {
                        return new OrderFragment();
                    }
                }
            }
        };

        fragmentStateManager.changeFragment(ORDER_FRAGMENT);
    }

    private void setupNavigation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        DrawerLayout drawer;
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_order: {
                fragmentStateManager.changeFragment(ORDER_FRAGMENT);
                break;
            }

            case R.id.nav_order_list: {
                fragmentStateManager.changeFragment(LIST_ORDER_FRAGMENT);
                break;
            }

            case R.id.nav_notification: {
                fragmentStateManager.changeFragment(ALERT_FRAGMENT);
                break;
            }

            case R.id.nav_help: {
                fragmentStateManager.changeFragment(HELP_FRAGMENT);
                break;
            }

            case R.id.nav_logout:{
                goToIntroScreen();
                break;
            }
            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
