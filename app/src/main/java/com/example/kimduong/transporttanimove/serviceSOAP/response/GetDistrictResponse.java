package com.example.kimduong.transporttanimove.serviceSOAP.response;

import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.DistrictResponse;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class GetDistrictResponse extends SOAPDeserializable {
    @JSoapResField(name = "huyen")
    public DistrictResponse[] districtRes;

    public DistrictResponse[] getDistrictRes() {
        return districtRes;
    }
}
