package com.example.kimduong.transporttanimove.ui.location;

import com.example.kimduong.transporttanimove.base.MvpView;

public interface MapsView extends MvpView {
    void modeInsertDonHang();
    void modeChiTietDonHang();
}
