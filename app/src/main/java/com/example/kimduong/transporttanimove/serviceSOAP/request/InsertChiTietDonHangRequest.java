package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class InsertChiTietDonHangRequest extends SOAPSerializable {


    @JSoapReqField(order = 0,fieldName ="IDDonHang")
    private Integer iDDonHang;
    
    @JSoapReqField(order = 0,fieldName ="maPhuongTienVanChuyen")
    private Integer maPhuongTienVanChuyen;
    
    @JSoapReqField(order = 0,fieldName ="maMaLoaiHang")
    private Integer maMaLoaiHang;
   
    @JSoapReqField(order = 0,fieldName ="maTinh")
    private Integer maTinh;
   
    @JSoapReqField(order = 0,fieldName ="maHuyen")
    private Integer maHuyen;
   
    @JSoapReqField(order = 0,fieldName ="maXaPhuong")
    private Integer maXaPhuong;
   
    @JSoapReqField(order = 0,fieldName ="diaChiDiemGiao")
    private String diaChiDiemGiao;
 
    @JSoapReqField(order = 0,fieldName ="toaDoDiemGiao")
    private String toaDoDiemGiao;
  
    @JSoapReqField(order = 0,fieldName ="khoangCach")
    private Integer khoangCach;
 
    @JSoapReqField(order = 0,fieldName ="ktChieuCao")
    private Integer ktChieuCao;
   
    @JSoapReqField(order = 0,fieldName ="ktChieuDai")
    private Integer ktChieuDai;
   
    @JSoapReqField(order = 0,fieldName ="ktChieuRong")
    private Integer ktChieuRong;
  
    @JSoapReqField(order = 0,fieldName ="canNang")
    private Integer canNang;
  
    @JSoapReqField(order = 0,fieldName ="soLuong")
    private Integer soLuong;
  
    @JSoapReqField(order = 0,fieldName ="ghiChu")
    private String ghiChu;
   
    @JSoapReqField(order = 0,fieldName ="lienHe")
    private String lienHe;

    public Integer getIDDonHang() {
        return iDDonHang;
    }

    public void setIDDonHang(Integer iDDonHang) {
        this.iDDonHang = iDDonHang;
    }

    public Integer getMaPhuongTienVanChuyen() {
        return maPhuongTienVanChuyen;
    }

    public void setMaPhuongTienVanChuyen(Integer maPhuongTienVanChuyen) {
        this.maPhuongTienVanChuyen = maPhuongTienVanChuyen;
    }

    public Integer getMaMaLoaiHang() {
        return maMaLoaiHang;
    }

    public void setMaMaLoaiHang(Integer maMaLoaiHang) {
        this.maMaLoaiHang = maMaLoaiHang;
    }

    public Integer getMaTinh() {
        return maTinh;
    }

    public void setMaTinh(Integer maTinh) {
        this.maTinh = maTinh;
    }

    public Integer getMaHuyen() {
        return maHuyen;
    }

    public void setMaHuyen(Integer maHuyen) {
        this.maHuyen = maHuyen;
    }

    public Integer getMaXaPhuong() {
        return maXaPhuong;
    }

    public void setMaXaPhuong(Integer maXaPhuong) {
        this.maXaPhuong = maXaPhuong;
    }

    public String getDiaChiDiemGiao() {
        return diaChiDiemGiao;
    }

    public void setDiaChiDiemGiao(String diaChiDiemGiao) {
        this.diaChiDiemGiao = diaChiDiemGiao;
    }

    public String getToaDoDiemGiao() {
        return toaDoDiemGiao;
    }

    public void setToaDoDiemGiao(String toaDoDiemGiao) {
        this.toaDoDiemGiao = toaDoDiemGiao;
    }

    public Integer getKhoangCach() {
        return khoangCach;
    }

    public void setKhoangCach(Integer khoangCach) {
        this.khoangCach = khoangCach;
    }

    public Integer getKtChieuCao() {
        return ktChieuCao;
    }

    public void setKtChieuCao(Integer ktChieuCao) {
        this.ktChieuCao = ktChieuCao;
    }

    public Integer getKtChieuDai() {
        return ktChieuDai;
    }

    public void setKtChieuDai(Integer ktChieuDai) {
        this.ktChieuDai = ktChieuDai;
    }

    public Integer getKtChieuRong() {
        return ktChieuRong;
    }

    public void setKtChieuRong(Integer ktChieuRong) {
        this.ktChieuRong = ktChieuRong;
    }

    public Integer getCanNang() {
        return canNang;
    }

    public void setCanNang(Integer canNang) {
        this.canNang = canNang;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getLienHe() {
        return lienHe;
    }

    public void setLienHe(String lienHe) {
        this.lienHe = lienHe;
    }


}
