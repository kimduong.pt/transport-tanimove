package com.example.kimduong.transporttanimove.ui.intro;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.example.kimduong.transporttanimove.ui.login.LoginActivity;
import com.example.kimduong.transporttanimove.ui.register.RegisterActivity;
import com.example.kimduong.transporttanimove.util.Constants;
import com.example.kimduong.transporttanimove.util.SharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroActivity extends BaseActivity  {
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.btn_sign_up)
    AppCompatButton btnSignUp;

    @Override
    protected void setUp() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        clearSharedPreferences();
        ButterKnife.bind(this);
//        TestServiceSOAP testServiceSOAP = new TestServiceSOAP();
//        testServiceSOAP.textMain();
    }

    private void clearSharedPreferences() {
        SharedPreferences sharedPreferences =  SharedPreferences.getInstance(this);
        sharedPreferences.removeValue(Constants.ID_CUSTOMER);
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_sign_up)
    public void onBtnSignUpClicked() {
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);
    }


}
