package com.example.kimduong.transporttanimove.ui.ListViewAddress;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseFragment;
import com.example.kimduong.transporttanimove.model.AddressDescription;
import com.example.kimduong.transporttanimove.model.eventBus.AddressEvents;
import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.CityResponse;
import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.DistrictResponse;
import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.WardResponse;
import com.example.kimduong.transporttanimove.serviceSOAP.service.SOAPServiceAuth;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetDistrictRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetWardRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ListAddressFragment extends BaseFragment implements AddressDescriptionAdapter.OnItemListener {
    @BindView(R.id.lv_address)
    RecyclerView lvAddress;
    private Unbinder unbinder;
    private List<AddressDescription> addressDescriptions;
    private AddressDescriptionAdapter addressDescriptionAdapter;
    private String field;
    public static final String FIELD_CITY = "city";
    public static final String FIELD_DISTRICT = "district";
    public static final String FIELD_WARD = "ward";
    private SOAPServiceAuth soapServiceAuth;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private AddressEvents addressEvents = new AddressEvents();


    @Override
    protected void setUp(View view) {
        addressDescriptions = new ArrayList<>();
        addressDescriptionAdapter = new AddressDescriptionAdapter(addressDescriptions);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        lvAddress.setLayoutManager(layoutManager);
        lvAddress.setItemAnimator(new DefaultItemAnimator());
        lvAddress.setAdapter(addressDescriptionAdapter);
        addressDescriptionAdapter.setOnItemListener(this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Timber.i("Detach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
        Timber.i("onViewCreated");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        soapServiceAuth = SOAPServiceAuth.getInstance();
        getCity();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getCity() {
        Disposable disposable = soapServiceAuth.getListCity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getCityResponse -> {
                    addressDescriptions.clear();
                    CityResponse[] cityRes = getCityResponse.getProvinceRes();
                    for (int i = 0; i < cityRes.length; i++) {
                        addressDescriptions.add(new AddressDescription(cityRes[i].getId(), cityRes[i].getProvinceName(), FIELD_CITY));
                    }
                    addressDescriptionAdapter.notifyDataSetChanged();
                }, Timber::e);
        compositeDisposable.add(disposable);
    }

    public void getWard(GetWardRequest getWardRequest) {
        Disposable disposable = soapServiceAuth.getWardResponseObservable(getWardRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getWardResponse -> {
                    addressDescriptions.clear();
                    WardResponse[] wardRes = getWardResponse.getWardRes();
                    for (int i = 0; i < wardRes.length; i++) {
                        addressDescriptions.add(new AddressDescription(wardRes[i].getId(), wardRes[i].getWardName(), FIELD_WARD));
                    }
                    addressDescriptionAdapter.notifyDataSetChanged();
                });
        compositeDisposable.add(disposable);

    }

    public void getDistrict(GetDistrictRequest getDistrictRequest) {
        Disposable disposable = soapServiceAuth.getDistrictResponseObservable(getDistrictRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getDistrictResponse -> {
                    DistrictResponse[] districtRes = getDistrictResponse.getDistrictRes();
                    addressDescriptions.clear();
                    for (int i = 0; i < districtRes.length; i++) {
                        addressDescriptions.add(new AddressDescription(districtRes[i].getId(), districtRes[i].getDistrictName(), FIELD_DISTRICT));
                    }
                    addressDescriptionAdapter.notifyDataSetChanged();
                }, Timber::e);
        compositeDisposable.add(disposable);

    }


    @Override
    public void getAddressFromList(AddressDescription addressDescription) {
        switch (addressDescription.getField()) {

            case FIELD_CITY: {
                addressEvents.setIdCity(addressDescription.getId());
                addressEvents.setNameCity(addressDescription.getName());
                getDistrict(new GetDistrictRequest(addressDescription.getId()));
                break;
            }

            case FIELD_DISTRICT: {
                addressEvents.setIdDistrict(addressDescription.getId());
                addressEvents.setNameDistrict(addressDescription.getName());
                getWard(new GetWardRequest(addressDescription.getId()));
                break;
            }

            case FIELD_WARD: {
                addressEvents.setIdWard(addressDescription.getId());
                addressEvents.setNameWard(addressDescription.getName());
                //Done
                EventBus.getDefault().post(addressEvents);
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commitNow();
                break;
            }
            default:break;
        }

    }

}
