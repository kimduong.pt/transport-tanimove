package com.example.kimduong.transporttanimove.ui.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.example.kimduong.transporttanimove.model.ManagementOrder;
import com.example.kimduong.transporttanimove.ui.order.OrderFragment;
import com.example.kimduong.transporttanimove.util.Constants;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlacesOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, MapsView {


    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.edt_subAddress)
    TextInputEditText edtSubAddress;
    @BindView(R.id.edt_name_contact)
    TextInputEditText edtNameContact;
    @BindView(R.id.edt_phone)
    TextInputEditText edtPhone;
    @BindView(R.id.edt_chieuDai)
    TextInputEditText edtChieuDai;
    @BindView(R.id.edt_chieuCao)
    TextInputEditText edtChieuCao;
    @BindView(R.id.edt_chieuRong)
    TextInputEditText edtChieuRong;
    @BindView(R.id.edt_canNang)
    TextInputEditText edtCanNang;
    @BindView(R.id.tv_soLuong)
    TextInputEditText tvSoLuong;
    @BindView(R.id.edt_noiDung)
    TextInputEditText edtNoiDung;
    @BindView(R.id.detailLayout)
    LinearLayout detailLayout;
    @BindView(R.id.btn_pick)
    Button btnPick;
    @BindView(R.id.ll_attribute_box)
    LinearLayout llAttributeBox;
    @BindView(R.id.ll_attribute_box_2)
    LinearLayout llAttributeBox2;
    @BindView(R.id.ll_checkbox)
    LinearLayout llCheckbox;


    private MapsPresenter mapsPresenter;

    private GoogleMap mMap;
    final RxPermissions rxPermissions = new RxPermissions(this);
    private boolean mLocationPermissionGranted;
    private String TAG = "MapsActivity";
    private GeoDataClient mGeoDataClient;
    private GoogleApi<PlacesOptions> mPlaceDetectionClient;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int DEFAULT_ZOOM = 20;

    private Location mLastKnownLocation;
    private LatLng mDefaultLocation = new LatLng(0.0, 0.0);
    private LatLng yourPosition;

    //AutoComplete
    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
    private static final int AUTOCOMPLETE_REQUEST_CODE = 102;
    private static final String API_KEY = "AIzaSyDRwMYR7WLAh35VgXB_l9VoRfI9MmjNbqs";
    private PublishSubject<LatLng> latLngObservable = PublishSubject.create();
    private boolean insertdonhangMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        prepareView();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getLocationPermission();
        init();
        getDeviceLocation();
        updateLocationUI();
        initializedAutoCompletePlace();
        gotoAutoComplePlace();


    }

    @Override
    protected void setUp() {

    }

    private void prepareView() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int anInt = bundle.getInt(Constants.MAP_VIEW_MODE);
            switch (anInt) {
                case OrderFragment.CHITIET_DONHANG:
                    this.modeChiTietDonHang();
                    break;
                case OrderFragment.INSERT_DONHANG:
                    this.modeInsertDonHang();
                    break;

                default:
                    //TODO implement Error
                    break;
            }
        }

        mapsPresenter = new MapsPresenter();
        mapsPresenter.attachView(this);

    }


    private void initializedAutoCompletePlace() {
        Places.initialize(this, API_KEY);
        PlacesClient placesClient = Places.createClient(this);
    }

    private void init() {
        // Construct a GeoDataClient.
//        mGeoDataClient = Places.getGeoDataClient(this, null);

        // Construct a PlaceDetectionClient.
//        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (yourPosition != null)
            mMap.addMarker(new MarkerOptions().position(yourPosition)
                    .title("Your Title")
                    .snippet("Please move the marker if needed.")
                    .draggable(true));

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (!mLocationPermissionGranted) {
            getLocationPermission();
        }
        mMap.setOnCameraChangeListener(cameraPosition -> {
            Timber.d(cameraPosition.toString());
            yourPosition = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
            this.latLngObservable.onNext(yourPosition);

        });

        configUIGMap();
        getDeviceLocation();
        updateLocationUI();
    }

    private void configUIGMap() {
//        mMap.getUiSettings().setMyLocationButtonEnabled(false);

    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setCompassEnabled(false);
                if (mLastKnownLocation == null) return;
                mMap.addMarker(new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())).title("Marker in Sydney"));
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        /**
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */

        if (mLocationPermissionGranted) {
            mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, location -> {
                if (location != null) {
                    mLastKnownLocation = location;
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(mLastKnownLocation.getLatitude(),
                                    mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                } else {
                    //Not Result location
                }
            });
        } else {
            // NOT granted permissions
        }

        Disposable subscribe = latLngObservable.debounce(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(latLng -> {
            setAddresstoTextField(latLngtoAddress(latLng));
            Timber.e("Fucking setUp");
        }, Timber::e, () -> {
        });
    }

    private void getLocationPermission() {
        Disposable disposable = rxPermissions
                .request(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> mLocationPermissionGranted = granted
                );
    }


    private void gotoAutoComplePlace() {
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("VN")
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Observable<Place> placeObservable = Observable.create(emitter -> {
            if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    emitter.onNext(place);
                    emitter.onComplete();
                    Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    // TODO: Handle the error.
                    Status status = Autocomplete.getStatusFromIntent(data);
                    emitter.onError(new Throwable(String.valueOf(status)));
                    Log.i(TAG, status.getStatusMessage());
                    emitter.onComplete();
                } else if (resultCode == RESULT_CANCELED) {
                    emitter.onComplete();
                    // The user canceled the operation.
                }
            }
        });

        Disposable disposable = placeObservable.subscribe(this::handlePlaceResult, this::handPlaceError, this::handPlaceComplete);

    }

    private void handlePlaceResult(Place place) {
        Timber.e(place.toString());
        LatLng string_location = place.getLatLng();
        String address = place.getAddress();
        String name = place.getName();
        mapCameraMove(string_location);
    }

    private void handPlaceError(Throwable throwable) {
        Timber.e(throwable);
    }

    private void handPlaceComplete() {

    }

    private void mapCameraMove(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
        try {
            latLngtoAddress(latLng);
            setAddresstoTextField(latLngtoAddress(latLng));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String latLngtoAddress(LatLng latLng) throws IOException {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String zip = addresses.get(0).getPostalCode();
        String country = addresses.get(0).getCountryName();
        Timber.e(addresses.get(0).toString());
        return addresses.get(0).getAddressLine(0);

    }

    private void setAddresstoTextField(String address) {
        Timber.e(address);
        tvSearch.setText(address);
    }

    @OnClick(R.id.tv_search)
    public void onViewClicked() {
        gotoAutoComplePlace();
    }

    @Override
    public void modeInsertDonHang() {
        insertdonhangMode = true;
        llAttributeBox.setVisibility(View.GONE);
        llAttributeBox2.setVisibility(View.GONE);

    }

    @Override
    public void modeChiTietDonHang() {
        insertdonhangMode = false;
        edtNoiDung.setVisibility(View.GONE);
        llCheckbox.setVisibility(View.GONE);

    }

    @OnClick(R.id.btn_pick)
    public void onPickLocation() {
        ManagementOrder managementOrder = ManagementOrder.getINSTANCE();
        if (insertdonhangMode) {
            //INSERT DON HANG

//            InsertDonHangRequest insertDonHangRequest = InsertDonHangRequestBuilder
//                    .anInsertDonHangRequest()
//                    .withDiaChiNhanHang()
//                    .withIDKhachHang()
//                    .withMaHuyenNhanHang()
//                    .withMaTinhNhanHang()
//                    .withMaXaPhuongNhanHang()
//                    .withToaDoNhanHang()
//                    .withGiaoTanTay()
//                    .withThuHo()
//                    .withNoiDungVanChuyen()
//                    .withThongTinLienHe()
//            managementOrder.setInsertDonHangRequest();


        } else {
            // CHI TIET DON HANG

//            InsertChiTietDonHangRequest request = InsertChiTietDonHangRequestBuilder
//                    .anInsertChiTietDonHangRequest()
//                    .withIDDonHang()
//                    .withCanNang()
//                    .withDiaChiDiemGiao()
//                    .withGhiChu()
//                    .withKtChieuCao()
//                    .withKtChieuDai()
//                    .withKtChieuRong()
//                    .withLienHe()
//                    .withMaHuyen()
//                    .withMaTinh()
//                    .withMaPhuongTienVanChuyen()
//                    .withToaDoDiemGiao()
//                    .build();



        }
    }
}