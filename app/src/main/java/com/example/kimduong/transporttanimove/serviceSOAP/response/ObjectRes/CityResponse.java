package com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class CityResponse extends SOAPDeserializable {
    @JSoapResField(name = "ID")
    public Integer id;
    @JSoapResField(name = "tenTinh")
    public String cityName;

    public Integer getId() {
        return id;
    }

    public String getProvinceName() {
        return cityName;
    }

    @Override
    public String toString() {
        return String.format("ID : %d - ProvinceName: %s \n ",id,cityName );
    }
}
