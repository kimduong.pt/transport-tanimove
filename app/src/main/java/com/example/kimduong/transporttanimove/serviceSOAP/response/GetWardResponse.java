package com.example.kimduong.transporttanimove.serviceSOAP.response;

import com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes.WardResponse;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;

public class GetWardResponse extends SOAPDeserializable {
    @JSoapResField(name = "xaPhuong")
    public WardResponse[] wardRes;

    public WardResponse[] getWardRes() {
        return wardRes;
    }
}
