package com.example.kimduong.transporttanimove.ui.orderList;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseFragment;

public class OrderListFragment extends BaseFragment {

    public OrderListFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.order_list_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setUp(view);
    }

    @Override
    protected void setUp(View view) {

    }
}
