package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class RegisterRequest extends SOAPSerializable {
    @JSoapReqField(order = 0, fieldName = "hoTen")
    String fullName;

    @JSoapReqField(order = 0, fieldName = "gioiTinh")
    String sex;

    @JSoapReqField(order = 0, fieldName = "tenDangNhap")
    String username;

    @JSoapReqField(order = 0, fieldName = "matKhau")
    String password;

    @JSoapReqField(order = 0, fieldName = "dienThoai")
    String phoneNumber;

    @JSoapReqField(order = 0, fieldName = "eMail")
    String email;

    @JSoapReqField(order = 0, fieldName = "diaChi")
    String address;

    @JSoapReqField(order = 0, fieldName = "IME")
    String IME;

    @JSoapReqField(order = 0, fieldName = "viTriDangKy")
    String viTriDangKy;

    @JSoapReqField(order = 0, fieldName = "viTriDangNhap")
    String viTriDangNhap;

    @JSoapReqField(order = 0, fieldName = "maTinh")
    int idCity;

    @JSoapReqField(order = 0, fieldName = "maHuyen")
    int idDistrict;

    @JSoapReqField(order = 0, fieldName = "maXaPhuong")
    int idWard;

    private RegisterRequest registerRequest;


    public RegisterRequest(String fullName, String sex, String username, String password, String phoneNumber, String email, String address, String IME, String viTriDangKy, String viTriDangNhap, int idCity, int idDistrict, int idWard) {
        this.fullName = fullName;
        this.sex = sex;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.IME = IME;
        this.viTriDangKy = viTriDangKy;
        this.viTriDangNhap = viTriDangNhap;
        this.idCity = idCity;
        this.idDistrict = idDistrict;
        this.idWard = idWard;
    }

    public RegisterRequest() {

    }

    public int getIdCity() {
        return idCity;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public int getIdWard() {
        return idWard;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIME(String IME) {
        this.IME = IME;
    }

    public void setViTriDangKy(String viTriDangKy) {
        this.viTriDangKy = viTriDangKy;
    }

    public void setViTriDangNhap(String viTriDangNhap) {
        this.viTriDangNhap = viTriDangNhap;
    }

    public void setIdCity(int idCity) {
        this.idCity = idCity;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public void setIdWard(int idWard) {
        this.idWard = idWard;
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "fullName='" + fullName + '\'' +
                ", sex='" + sex + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", IME='" + IME + '\'' +
                ", viTriDangKy='" + viTriDangKy + '\'' +
                ", viTriDangNhap='" + viTriDangNhap + '\'' +
                ", idCity=" + idCity +
                ", idDistrict=" + idDistrict +
                ", idWard=" + idWard +
                ", registerRequest=" + registerRequest +
                '}';
    }
}

//<hoTen>string</hoTen>
//<gioiTinh>string</gioiTinh>
//<tenDangNhap>string</tenDangNhap>
//<matKhau>string</matKhau>
//<dienThoai>string</dienThoai>
//<eMail>string</eMail>
//<diaChi>string</diaChi>
//<IME>string</IME>
//<viTriDangKy>string</viTriDangKy>
//<viTriDangNhap>string</viTriDangNhap>
//<maTinh>int</maTinh>
//<maHuyen>int</maHuyen>
