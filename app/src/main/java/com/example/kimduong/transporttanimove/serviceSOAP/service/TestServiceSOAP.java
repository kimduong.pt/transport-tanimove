package com.example.kimduong.transporttanimove.serviceSOAP.service;

import android.annotation.SuppressLint;

import com.example.kimduong.transporttanimove.serviceSOAP.request.GetDistrictRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetIDKhachHangRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.GetWardRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.InsertDonHangRequest;
import com.example.kimduong.transporttanimove.serviceSOAP.request.RegisterRequest;

import java.util.Arrays;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class TestServiceSOAP {
    private SOAPServiceAuth soapServiceAuth;
    private SOAPServiceOrder serviceOrder;

    @SuppressLint("CheckResult")
    public TestServiceSOAP() {
        soapServiceAuth = SOAPServiceAuth.getInstance();
        serviceOrder = SOAPServiceOrder.getInstance();
    }

    public void test() {

        soapServiceAuth.getListCity()
                .subscribeOn(Schedulers.io())
                .subscribe(provinceResponse -> Timber.e(Arrays.deepToString(provinceResponse.getProvinceRes()))
                        , Timber::e);

        soapServiceAuth.getDistrictResponseObservable(new GetDistrictRequest(79))
                .subscribeOn(Schedulers.io())
                .subscribe(cityResponse -> Timber.e(Arrays.deepToString(cityResponse.getDistrictRes()))
                        , Timber::e);

        soapServiceAuth.getWardResponseObservable(new GetWardRequest(760))
                .subscribeOn(Schedulers.io())
                .subscribe(getWardResponse -> Timber.e(Arrays.deepToString(getWardResponse.getWardRes()))
                        , Timber::e);

        soapServiceAuth.register(new RegisterRequest("HoangDuong",
                "Nam",
                "kimduongpt",
                "123456",
                "01676149414",
                "kimduong@abc.com",
                "ALoAlo",
                "nul",
                "nul",
                "nul",
                79, 760, 26737)).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(integer -> {
            Timber.e(String.format("Result Register : %d", integer));
        }, Timber::e);
    }

    public void testGetIdKhachHang() {

        Disposable disposable = soapServiceAuth.getIdKhachHang(new GetIDKhachHangRequest("kimduong"))
                .subscribeOn(Schedulers.io())
                .subscribe(integer -> Timber.e("GetIdKhachHang Fucking %d", integer), Timber::e);
    }

    public void tessInserDonHang() {
        InsertDonHangRequest i = new InsertDonHangRequest();
        i.setIDKhachHang(6);
        i.setDiaChiNhanHang("Hem 123, phuong abc, Quan 10");
        i.setGiaoTanTay(true);
        i.setMaTinhNhanHang(79);
        i.setMaHuyenNhanHang(765);
        i.setMaXaPhuongNhanHang(0);
        i.setNoiDungVanChuyen("khong co gi");
        i.setThongTinLienHe("khong co gi");
        i.setThuHo(true);

        Disposable disposable = serviceOrder.insertDonHang(i).subscribeOn(Schedulers.io())
                .subscribe(
                aBoolean -> {
                    Timber.e("Hello======= Fucking");
                    Timber.e(String.valueOf(aBoolean));
                }, Timber::e);
    }


    public void textMain() {
        testGetIdKhachHang();
//        tessInserDonHang();
    }
}
