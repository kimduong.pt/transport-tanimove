package com.example.kimduong.transporttanimove.base;

/**
 * Created by R4inV on 20/08/2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();

    void setUserAsLoggedOut();
}
