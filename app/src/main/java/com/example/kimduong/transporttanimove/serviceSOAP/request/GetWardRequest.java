package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class GetWardRequest extends SOAPSerializable {
    @JSoapReqField(order = 0,fieldName = "idHuyen")
    int idCity;

    public GetWardRequest(int idCity) {
        this.idCity = idCity;
    }
}
