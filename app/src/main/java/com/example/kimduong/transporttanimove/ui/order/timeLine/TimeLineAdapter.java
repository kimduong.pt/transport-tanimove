package com.example.kimduong.transporttanimove.ui.order.timeLine;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.util.VectorDrawableUtils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {
    private final int START_DELEVERY = 0;
    private final int LIMIT_DELEVERY = 2;
    private List<TimeLineOrderModel> mList = new ArrayList<>();
    private final int FOOTER = 2;
    private OnClickTimeLine onClickTimeLine;


    @NonNull
    @Override
    public TimeLineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;
        view = inflater.inflate(R.layout.item_timeline, viewGroup, false);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeLineViewHolder holder, int i) {
        TimeLineOrderModel timeLineOrderModel = mList.get(i);
//        holder.tvTitle.setText(timeLineOrderModel.getTitle());
//        holder.tvContent.setText(timeLineOrderModel.getContent());
        setMarker(holder,
                R.drawable.ic_gps_fixed_black_24dp,
                R.color.colorShadow_200);

        if (i == START_DELEVERY) {
            //Start
            holder.tvTitle.setText("Địa Điểm Nhận Hàng");
            holder.tvContent.setText("Nhấn vào để chọn địa điểm");
            holder.mTimelineView.setStartLineColor(R.color.colorPrimary, 1);
            holder.mTimelineView.setEndLineColor(R.color.colorPrimary, 1);
            if(onClickTimeLine != null)holder.cardView.setOnClickListener(v -> onClickTimeLine.onClickStartOrder());
        } else if (i == mList.size() - 1) {
            //END
            holder.tvTitle.setText("Địa Điểm Trả Hàng");
            holder.tvContent.setText("Nhấn vào để thêm địa điểm");
            holder.mTimelineView.setStartLineColor(R.color.colorPrimary, 2);
            holder.mTimelineView.setEndLineColor(R.color.colorPrimary, 2);
            if(onClickTimeLine != null)holder.cardView.setOnClickListener(v -> onClickTimeLine.onClickAddOrder());

        } else {
            //Body
            holder.tvTitle.setText("Địa Điểm Trả Hàng");
            holder.tvContent.setText(timeLineOrderModel.getContent());
            holder.mTimelineView.setStartLineColor(R.color.colorPrimary, 0);
            holder.mTimelineView.setEndLineColor(R.color.colorPrimary, 0);
            if(onClickTimeLine != null)holder.cardView.setOnClickListener(v -> onClickTimeLine.onClickEditOrder());

        }

    }

    public void setOnClickTimeLine(OnClickTimeLine onClickTimeLine) {
        this.onClickTimeLine = onClickTimeLine;
    }

    private void setMarker(TimeLineViewHolder holder, int drawableResId, int colorFilter) {

        Drawable drawable = VectorDrawableUtils.INSTANCE.getDrawable(holder.itemView.getContext(),
                drawableResId,
                ContextCompat.getColor(holder.itemView.getContext(),
                        colorFilter));

        holder.mTimelineView.setMarker(drawable);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setmList(List<TimeLineOrderModel> mList) {
        this.mList = mList;
        mList.add(new TimeLineOrderModel("FOOTER"));//Add Footer
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    class TimeLineViewHolder extends RecyclerView.ViewHolder {
        TimelineView mTimelineView;
        TextView tvTitle;
        TextView tvContent;
        CardView cardView;


        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);
            mTimelineView = itemView.findViewById(R.id.timeline);
            mTimelineView.initLine(viewType);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvContent = (TextView) itemView.findViewById(R.id.tv_content);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    //    public class FooterHolder extends TimeLineViewHolder {
//        public TimelineView mTimelineView;
//        public ImageView imageView;
//
//        public FooterHolder(View itemView, int viewType) {
//            super(itemView,viewType);
//            mTimelineView = itemView.findViewById(R.id.timeline_add);
//            mTimelineView.initLine(viewType);
//           imageView = itemView.findViewById(R.id.btn_add_order);
//        }
//    }
    public interface OnClickTimeLine {
        void onClickStartOrder();
        void onClickEditOrder();
        void onClickAddOrder();
    }
}
