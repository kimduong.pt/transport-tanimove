package com.example.kimduong.transporttanimove.ui.register;

import com.example.kimduong.transporttanimove.base.BaseMvpPresenter;
import com.example.kimduong.transporttanimove.serviceSOAP.service.SOAPServiceAuth;
import com.example.kimduong.transporttanimove.serviceSOAP.request.RegisterRequest;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RegisterPresenter extends BaseMvpPresenter<RegisterView> {
    private SOAPServiceAuth soapServiceAuth = SOAPServiceAuth.getInstance();
    private RegisterRequest registerRequest = new RegisterRequest();

    public RegisterPresenter(){

    }

    public void register(){
        Timber.d(registerRequest.toString());
        getCompositeDisposable().add(soapServiceAuth.register(registerRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(results ->{
                    switch (results){
                        case "1":{
                            getMvpView().showSimpleDialog("Thành Công", "Chúc Mừng Bạn Đã Đăng Kí Thành Công",(dialog, which) ->
                            getMvpView().finishActivity());

                        }
                        default: {
                            break;
                          //  registerNotSuccessfully();
                        }
                    }
                },this::handleError));
    }

    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }

    public void registerNotSuccessfully() {
        getMvpView().showSimpleDialog("Lỗi","Đăng Kí Không Thành Công",null);
    }

}
