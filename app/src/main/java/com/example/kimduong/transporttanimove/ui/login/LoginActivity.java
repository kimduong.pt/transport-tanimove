package com.example.kimduong.transporttanimove.ui.login;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kimduong.transporttanimove.MainActivity;
import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.example.kimduong.transporttanimove.ui.register.RegisterActivity;
import com.example.kimduong.transporttanimove.util.Constants;
import com.example.kimduong.transporttanimove.util.SharedPreferences;
import com.example.kimduong.transporttanimove.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {
    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.link_signup)
    TextView linkSignup;
    @BindView(R.id.link_forget_password)
    TextView linkForgetPassword;
    private LoginPresenter loginPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setUp();
    }

    @Override
    protected void setUp() {
        loginPresenter = new LoginPresenter();
        loginPresenter.attachView(this);
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        String username = inputUsername.getText().toString();
        String password = inputPassword.getText().toString();
        if (Utils.isNotNullOrEmpty(username) && Utils.isNotNullOrEmpty(password)) {
            loginPresenter.signIn(username, password);
        }
    }

    @OnClick(R.id.link_signup)
    public void onLinkSignUpClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.link_forget_password)
    public void onLinkForgetPasswordClicked() {
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @Override
    public void saveIDSharePre(int id) {
        SharedPreferences sharedPreferences = SharedPreferences.getInstance(this);
        sharedPreferences.saveInteger(Constants.ID_CUSTOMER, id);
        this.goToMainActivity();
    }


}
