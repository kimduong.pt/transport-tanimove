package com.example.kimduong.transporttanimove.serviceSOAP.response.ObjectRes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pt.joaocruz04.lib.annotations.JSoapResField;
import pt.joaocruz04.lib.misc.SOAPDeserializable;


/**
 * {@link com.example.kimduong.transporttanimove.R.xml#list_don_hang}
 *
 * */
public class DonHangResponse extends SOAPDeserializable {

    @JSoapResField(name ="ID")
    private Integer iD;

    @JSoapResField(name ="IDKhachHang")
    private Integer iDKhachHang;

    @JSoapResField(name ="maTinhNhanHang")
    private Integer maTinhNhanHang;
    @JSoapResField(name ="maHuyenNhanHang")

    private Integer maHuyenNhanHang;
    @JSoapResField(name ="maXaPhuongNhanHang")

    private Integer maXaPhuongNhanHang;
    @JSoapResField(name ="diaChiNhanHang")

    private String diaChiNhanHang;
    @JSoapResField(name ="toaDoNhanHang")

    private String toaDoNhanHang;
    @JSoapResField(name ="thongTinLienHe")

    private String thongTinLienHe;
    @JSoapResField(name ="IDNguoiNhan")

    private Integer iDNguoiNhan;
    @JSoapResField(name ="tenNguoiNhan")

    private String tenNguoiNhan;
    @JSoapResField(name ="IDnguoiGiao")

    private Integer iDnguoiGiao;
    @JSoapResField(name ="tenNguoiGiao")

    private String tenNguoiGiao;
    @JSoapResField(name ="noiDungVanChuyen")

    private String noiDungVanChuyen;
    @JSoapResField(name ="thoiGianTaoDonHang")

    private String thoiGianTaoDonHang;
    @JSoapResField(name ="thoiGianNhan")

    private String thoiGianNhan;
    @JSoapResField(name ="thoiGianKetThucGiao")

    private String thoiGianKetThucGiao;
    @JSoapResField(name ="tongTienVanChuyen")

    private String tongTienVanChuyen;
    @JSoapResField(name ="trangThaiDonHang")

    private Integer trangThaiDonHang;
    @JSoapResField(name ="thuHo")

    private Boolean thuHo;
    @JSoapResField(name ="giaoTanTay")

    private Boolean giaoTanTay;
    @JSoapResField(name ="tienThuHo")

    private String tienThuHo;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getIDKhachHang() {
        return iDKhachHang;
    }

    public void setIDKhachHang(Integer iDKhachHang) {
        this.iDKhachHang = iDKhachHang;
    }

    public Integer getMaTinhNhanHang() {
        return maTinhNhanHang;
    }

    public void setMaTinhNhanHang(Integer maTinhNhanHang) {
        this.maTinhNhanHang = maTinhNhanHang;
    }

    public Integer getMaHuyenNhanHang() {
        return maHuyenNhanHang;
    }

    public void setMaHuyenNhanHang(Integer maHuyenNhanHang) {
        this.maHuyenNhanHang = maHuyenNhanHang;
    }

    public Integer getMaXaPhuongNhanHang() {
        return maXaPhuongNhanHang;
    }

    public void setMaXaPhuongNhanHang(Integer maXaPhuongNhanHang) {
        this.maXaPhuongNhanHang = maXaPhuongNhanHang;
    }

    public String getDiaChiNhanHang() {
        return diaChiNhanHang;
    }

    public void setDiaChiNhanHang(String diaChiNhanHang) {
        this.diaChiNhanHang = diaChiNhanHang;
    }

    public String getToaDoNhanHang() {
        return toaDoNhanHang;
    }

    public void setToaDoNhanHang(String toaDoNhanHang) {
        this.toaDoNhanHang = toaDoNhanHang;
    }

    public String getThongTinLienHe() {
        return thongTinLienHe;
    }

    public void setThongTinLienHe(String thongTinLienHe) {
        this.thongTinLienHe = thongTinLienHe;
    }

    public Integer getIDNguoiNhan() {
        return iDNguoiNhan;
    }

    public void setIDNguoiNhan(Integer iDNguoiNhan) {
        this.iDNguoiNhan = iDNguoiNhan;
    }

    public String getTenNguoiNhan() {
        return tenNguoiNhan;
    }

    public void setTenNguoiNhan(String tenNguoiNhan) {
        this.tenNguoiNhan = tenNguoiNhan;
    }

    public Integer getIDnguoiGiao() {
        return iDnguoiGiao;
    }

    public void setIDnguoiGiao(Integer iDnguoiGiao) {
        this.iDnguoiGiao = iDnguoiGiao;
    }

    public String getTenNguoiGiao() {
        return tenNguoiGiao;
    }

    public void setTenNguoiGiao(String tenNguoiGiao) {
        this.tenNguoiGiao = tenNguoiGiao;
    }

    public String getNoiDungVanChuyen() {
        return noiDungVanChuyen;
    }

    public void setNoiDungVanChuyen(String noiDungVanChuyen) {
        this.noiDungVanChuyen = noiDungVanChuyen;
    }

    public String getThoiGianTaoDonHang() {
        return thoiGianTaoDonHang;
    }

    public void setThoiGianTaoDonHang(String thoiGianTaoDonHang) {
        this.thoiGianTaoDonHang = thoiGianTaoDonHang;
    }

    public String getThoiGianNhan() {
        return thoiGianNhan;
    }

    public void setThoiGianNhan(String thoiGianNhan) {
        this.thoiGianNhan = thoiGianNhan;
    }

    public String getThoiGianKetThucGiao() {
        return thoiGianKetThucGiao;
    }

    public void setThoiGianKetThucGiao(String thoiGianKetThucGiao) {
        this.thoiGianKetThucGiao = thoiGianKetThucGiao;
    }

    public String getTongTienVanChuyen() {
        return tongTienVanChuyen;
    }

    public void setTongTienVanChuyen(String tongTienVanChuyen) {
        this.tongTienVanChuyen = tongTienVanChuyen;
    }

    public Integer getTrangThaiDonHang() {
        return trangThaiDonHang;
    }

    public void setTrangThaiDonHang(Integer trangThaiDonHang) {
        this.trangThaiDonHang = trangThaiDonHang;
    }

    public Boolean getThuHo() {
        return thuHo;
    }

    public void setThuHo(Boolean thuHo) {
        this.thuHo = thuHo;
    }

    public Boolean getGiaoTanTay() {
        return giaoTanTay;
    }

    public void setGiaoTanTay(Boolean giaoTanTay) {
        this.giaoTanTay = giaoTanTay;
    }

    public String getTienThuHo() {
        return tienThuHo;
    }

    public void setTienThuHo(String tienThuHo) {
        this.tienThuHo = tienThuHo;
    }

    @Override
    public String toString() {
        return "DonHangResponse{" +
                "iD=" + iD +
                ", iDKhachHang=" + iDKhachHang +
                ", maTinhNhanHang=" + maTinhNhanHang +
                ", maHuyenNhanHang=" + maHuyenNhanHang +
                ", maXaPhuongNhanHang=" + maXaPhuongNhanHang +
                ", diaChiNhanHang='" + diaChiNhanHang + '\'' +
                ", toaDoNhanHang='" + toaDoNhanHang + '\'' +
                ", thongTinLienHe='" + thongTinLienHe + '\'' +
                ", iDNguoiNhan=" + iDNguoiNhan +
                ", tenNguoiNhan='" + tenNguoiNhan + '\'' +
                ", iDnguoiGiao=" + iDnguoiGiao +
                ", tenNguoiGiao='" + tenNguoiGiao + '\'' +
                ", noiDungVanChuyen='" + noiDungVanChuyen + '\'' +
                ", thoiGianTaoDonHang='" + thoiGianTaoDonHang + '\'' +
                ", thoiGianNhan='" + thoiGianNhan + '\'' +
                ", thoiGianKetThucGiao='" + thoiGianKetThucGiao + '\'' +
                ", tongTienVanChuyen='" + tongTienVanChuyen + '\'' +
                ", trangThaiDonHang=" + trangThaiDonHang +
                ", thuHo=" + thuHo +
                ", giaoTanTay=" + giaoTanTay +
                ", tienThuHo='" + tienThuHo + '\'' +
                '}';
    }

}

class ListDonHangResponse extends SOAPDeserializable{
    @JSoapResField(name = "donHang")
    DonHangResponse[] donHangRes ;

    public List<DonHangResponse> getDonHangList(){
        return new ArrayList<>(Arrays.asList(donHangRes));
    }
}
