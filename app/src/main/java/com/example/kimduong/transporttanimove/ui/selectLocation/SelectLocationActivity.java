package com.example.kimduong.transporttanimove.ui.selectLocation;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import butterknife.ButterKnife;
import timber.log.Timber;

public class SelectLocationActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        ButterKnife.bind(this);
    }

    @Override
    protected void setUp() {
        if(isCheckGoogleService()){
            initGoogleService();
        }
    }

    private void initGoogleService() {

    }


    boolean isCheckGoogleService(){
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if(available == ConnectionResult.SUCCESS){


            return true;
        }else {
            Timber.e("Google Service not available");
        }

        return false;
    }
}
