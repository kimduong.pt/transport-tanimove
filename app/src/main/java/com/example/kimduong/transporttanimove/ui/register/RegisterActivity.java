package com.example.kimduong.transporttanimove.ui.register;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.widget.AppCompatButton;

import com.example.kimduong.transporttanimove.R;
import com.example.kimduong.transporttanimove.base.BaseActivity;
import com.example.kimduong.transporttanimove.model.eventBus.AddressEvents;
import com.example.kimduong.transporttanimove.ui.ListViewAddress.ListAddressFragment;
import com.example.kimduong.transporttanimove.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterView {
    @BindView(R.id.input_layout_fullname)
    TextInputLayout inputLayoutFullname;
    @BindView(R.id.input_fullname)
    TextInputEditText inputFullname;
    @BindView(R.id.input_layout_username)
    TextInputLayout inputLayoutUsername;
    @BindView(R.id.input_username)
    TextInputEditText inputUsername;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;
    @BindView(R.id.input_layout_retype_password)
    TextInputLayout inputLayoutRetypePassword;
    @BindView(R.id.input_retype_password)
    TextInputEditText inputRetypePassword;
    @BindView(R.id.input_layout_address)
    TextInputLayout inputLayoutAddress;
    @BindView(R.id.input_address)
    TextInputEditText inputAddress;
    @BindView(R.id.input_layout_phone_number)
    TextInputLayout inputLayoutPhoneNumber;
    @BindView(R.id.input_phone_number)
    TextInputEditText inputPhoneNumber;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.btn_register)
    AppCompatButton btnRegister;
    @BindView(R.id.btn_sel_address)
    AppCompatButton btnSelAddress;

    private RegisterPresenter registerPresenter;
    private TextInputEditText[] textInputEditTexts;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_information);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        setUp();
    }

    @Override
    protected void setUp() {
        registerPresenter = new RegisterPresenter();
        registerPresenter.attachView(this);
        textInputEditTexts = new TextInputEditText[]{
                inputAddress,
                inputEmail,
                inputFullname,
                inputPassword,
                inputRetypePassword,
                inputPhoneNumber,
                inputUsername};


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        if (Utils.validateTextInputEditTexts(textInputEditTexts)) {
            //check password
            if (!inputPassword.getText().toString().equals(inputRetypePassword.getText().toString())) {
                showSimpleDialog("Lỗi", "Password Không Giống Nhau", null);
                return;
            }

            //validUserName

            if (!Utils.isValidUserName(inputUsername.getText().toString())) {
                showSimpleDialog("Lỗi", "Tên Đăng Nhập Không Hợp Lệ", null);
                return;
            }

            if (registerPresenter.getRegisterRequest().getIdCity() < 0) {
                showSimpleDialog("Lỗi", "Bạn chưa chọn địa chỉ", null);
                return;
            }

            setRegisterRequest();

            registerPresenter.register();

        } else {
            registerPresenter.registerNotSuccessfully();
        }

    }

    private void setRegisterRequest() {
        registerPresenter.getRegisterRequest().setAddress(inputAddress.getText().toString());

        registerPresenter.getRegisterRequest().setPassword(inputPassword.getText().toString());

        registerPresenter.getRegisterRequest().setFullName(inputFullname.getText().toString());

        registerPresenter.getRegisterRequest().setEmail(inputEmail.getText().toString());

        registerPresenter.getRegisterRequest().setPhoneNumber(inputPhoneNumber.getText().toString());

        registerPresenter.getRegisterRequest().setUsername(inputUsername.getText().toString());

        registerPresenter.getRegisterRequest().setSex("0");

        registerPresenter.getRegisterRequest().setIME("null");

        registerPresenter.getRegisterRequest().setViTriDangKy("null");

        registerPresenter.getRegisterRequest().setViTriDangNhap("null");
    }

    @OnClick(R.id.btn_sel_address)
    public void onSelectAddress() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListAddressFragment()).commitNow();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventAddress(AddressEvents addressEvents) {
        registerPresenter.getRegisterRequest().setIdCity(addressEvents.getIdCity());
        registerPresenter.getRegisterRequest().setIdDistrict(addressEvents.getIdDistrict());
        registerPresenter.getRegisterRequest().setIdWard(addressEvents.getIdWard());
        StringBuilder sb = new StringBuilder();
        sb.append(addressEvents.getNameWard());
        sb.append(", ");
        sb.append(addressEvents.getNameDistrict());
        sb.append(", ");
        sb.append(addressEvents.getNameCity());
        sb.append(".");
        btnSelAddress.setText(sb.toString());
    }

    @Override
    public void finishActivity() {
        finish();
    }
}
