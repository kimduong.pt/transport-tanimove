package com.example.kimduong.transporttanimove.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import com.google.android.material.textfield.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.example.kimduong.transporttanimove.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {
    public static Dialog getWaitingDialog(Activity c) {
        Dialog ret = new Dialog(c);
        LayoutInflater inf = ((LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        View v = inf.inflate(R.layout.waiting_dialog_layout, null);
        ret.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ret.setContentView(v);
        ret.setCanceledOnTouchOutside(false);
        ret.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ret.getWindow().setDimAmount(0.5f);
        ret.setCancelable(false);
        ret.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        return ret;
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static void showSimpleDialog(Context context, String title, String message, DialogInterface.OnClickListener callbackOk) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), callbackOk).show();
    }

    public static void showYesNoDialog(Context context, String title, String message,
                                       DialogInterface.OnClickListener callbackOk,
                                       DialogInterface.OnClickListener callbackCancel) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(context.getString(R.string.apply), callbackOk)
                .setPositiveButton(context.getString(R.string.cancel), callbackCancel)
                .show();
    }

    public static boolean isEmulator() {

        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static int parseBooleanToInt(boolean bool) {
        if (bool)
            return 1;
        return 0;
    }

    public static boolean parseIntToBoolean(int value) {
        return value == 1;
    }


    public static File saveImageAvatar(Bitmap bitmap) {
        bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, false);
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/avatar_smart_home");
        myDir.mkdir();
        String fileName = System.currentTimeMillis() + ".png";
        File imageFile = new File(myDir, fileName);
        try {
            OutputStream fOut = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageFile;
    }

//    public static String convertUnixTimeToDate(long unixtime) {
//        Date date = new Date(unixtime * 1000L);
//        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
//        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
//        return sdf.format(date);
//    }

    public static String convertUnixTimeToSecond(long unixtime) {
        Date date = new Date(unixtime * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
//        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }

    public static String stringToMD5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String implodeArrayUsingForLoop(String[] arrayToImplode, String separator) {

        if (arrayToImplode.length == 0) {
            return "";
        }

        if (arrayToImplode.length < 2) {
            return "\"" + arrayToImplode[0] + "\"";
        }

        StringBuilder stringbuffer = new StringBuilder();
        for (int i = 0; i < arrayToImplode.length; i++) {
            if (i != 0) stringbuffer.append(separator);
            stringbuffer.append("\"").append(arrayToImplode[i]).append("\"");
        }

        //return the implode string
        return stringbuffer.toString();
    }

    public static boolean isNumberValid(String number) {
        String regexStr = "^[0-9]*$";
        return !number.trim().matches(regexStr);
    }

    public static boolean isNumberPhone(String number) {
        String regexPhone = "(0[123456789])[0-9]{8,}";
        return number.trim().matches(regexPhone);
    }

    public static boolean isVietnamCountryCode(String phoneNumber) {
        return !phoneNumber.substring(0, 2).equals("84");
    }

    public static boolean isFirstNumberIs0(String phoneNumber) {
        return !phoneNumber.substring(0, 1).equals("0");
    }

    public static String formatStringAsPhoneNumber(String input) {
        String output;
        input = input.substring(1);
        switch (input.length()) {
            case 7:
                output = String.format("%s-%s", input.substring(0, 3), input.substring(3, 7));
                break;
            case 10:
                output = String.format("%s %s-%s", input.substring(0, 3), input.substring(3, 6), input.substring(6, 10));
                break;
            case 11:
                output = String.format("+%s %s-%s-%s", input.substring(0, 2), input.substring(2, 4), input.substring(4, 7), input.substring(7, 11));
                break;
            case 12:
                output = String.format("+%s %s-%s-%s", input.substring(0, 2), input.substring(2, 5), input.substring(5, 8), input.substring(8, 12));
                break;
            default:
                return input;
        }
        return output;
    }

    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean validateLetters(String txt) {
        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return !matcher.find();
    }

    public static boolean isValidUserName(String string){
        return string.matches(Constants.REG_USERNAME);
    }

    public static boolean isMoreTwoWord(String input) {
        String[] splitter = null;
        // string with 2 words and space
        splitter = input.trim().split(" ");
        return splitter.length >= 2;
    }

//    public static long getCurrentUnixTimeSystem() {
//        return DateTimeUtils.currentTimeMillis() / 1000L;
//    }

    public static boolean isFirstAndLastNameTooLong(String full_name) {
        String firstName = null, lastName = null;
        if (full_name.split("\\w+").length > 1) {
            lastName = full_name.substring(full_name.lastIndexOf(" ") + 1);
            firstName = full_name.substring(0, full_name.lastIndexOf(' '));
        }

        boolean first_name = lastName.length() > 30;
        boolean last_name = firstName.length() > 30;
        return first_name || last_name;
    }

    public static boolean isNotNullOrEmpty(String string) {
        if (string != null && !string.isEmpty()) {
            return true;
        } else
            return false;
    }

    public static boolean validateTextInputEditTexts(TextInputEditText[] fields){
        for(int i = 0; i < fields.length; i++){
            TextInputEditText currentField = fields[i];
            if(!isNotNullOrEmpty(currentField.getText().toString())){
                return false;
            }
        }
        return true;
    }

}
