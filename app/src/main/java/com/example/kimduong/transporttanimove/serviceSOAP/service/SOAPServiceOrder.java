package com.example.kimduong.transporttanimove.serviceSOAP.service;

import com.example.kimduong.transporttanimove.serviceSOAP.request.InsertDonHangRequest;

import io.reactivex.Observable;
import pt.joaocruz04.lib.SOAPManager;
import pt.joaocruz04.lib.misc.JSoapCallback;
import timber.log.Timber;

public class SOAPServiceOrder extends SoapHelper {
    private static SOAPServiceOrder INSTANCE;

    private SOAPServiceOrder() {
    }

    public static SOAPServiceOrder getInstance() {
        if(INSTANCE ==null){
            INSTANCE = new SOAPServiceOrder();
        }
        return INSTANCE;
    }

    public Observable<Boolean> insertDonHang(InsertDonHangRequest insertDonHangRequest){
        String action = "InsertDonHang";
        return Observable.create(emitter ->{
            SOAPManager.get(NAMESPACE, URL, action, getAction(action), insertDonHangRequest, Integer.class, new JSoapCallback() {
                @Override
                public void onSuccess(Object o) {
                    Timber.e(String.valueOf((int)o));
                    int idDonHang = (int)o;
                   if(idDonHang > 0){
                       emitter.onNext(Boolean.valueOf(Boolean.TRUE));
                   }else {
                       emitter.onNext(Boolean.valueOf(Boolean.FALSE));
                   }

                }

                @Override
                public void onError(int i) {
                    emitter.onError(new Throwable());
                }

                @Override
                public void onDebugMessage(String title, String message) {
                    super.onDebugMessage(title, message);
                    Timber.d("Title : %s, Message : %s", title, message);


                }
            });
        });
    }
}
