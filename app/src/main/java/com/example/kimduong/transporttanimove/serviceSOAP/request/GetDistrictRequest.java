package com.example.kimduong.transporttanimove.serviceSOAP.request;


import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class GetDistrictRequest extends SOAPSerializable {
    @JSoapReqField(order = 0,fieldName = "idTinh")
    int idCity;

    public GetDistrictRequest(int idCity) {
        this.idCity = idCity;
    }
}
