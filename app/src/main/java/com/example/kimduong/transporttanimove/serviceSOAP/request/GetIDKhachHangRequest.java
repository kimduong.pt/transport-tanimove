package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class GetIDKhachHangRequest extends SOAPSerializable {

    @JSoapReqField(order = 0,fieldName = "tenDangNhap")
    String tenDangNhap;

    public GetIDKhachHangRequest(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }
}
