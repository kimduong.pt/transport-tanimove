package com.example.kimduong.transporttanimove.serviceSOAP.request;

import com.example.kimduong.transporttanimove.serviceSOAP.service.SoapHelper;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;

@JSoapClass(namespace = SoapHelper.NAMESPACE)
public class UpdateProfileRequest extends SOAPSerializable {
    @JSoapReqField(order = 0, fieldName = "hoTen")
    String fullName;

    @JSoapReqField(order = 0, fieldName = "gioiTinh")
    String sex;

    @JSoapReqField(order = 0, fieldName = "dienThoai")
    String phoneNumber;

    @JSoapReqField(order = 0, fieldName = "eMail")
    String email;

    @JSoapReqField(order = 0, fieldName = "diaChi")
    String address;

    @JSoapReqField(order = 0, fieldName = "maTinh")
    int idCity;

    @JSoapReqField(order = 0, fieldName = "maHuyen")
    int idDistrict;

    @JSoapReqField(order = 0, fieldName = "maXaPhuong")
    int idWard;

    @JSoapReqField(order = 0, fieldName = "tenDangNhap")
    String username;

    public UpdateProfileRequest(String fullName, String sex, String phoneNumber, String email, String address, int idCity, int idDistrict, int idWard, String username) {
        this.fullName = fullName;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.idCity = idCity;
        this.idDistrict = idDistrict;
        this.idWard = idWard;
        this.username = username;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIdCity(int idCity) {
        this.idCity = idCity;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public void setIdWard(int idWard) {
        this.idWard = idWard;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
//<hoTen>string</hoTen>
//<gioiTinh>string</gioiTinh>
//<dienThoai>string</dienThoai>
//<eMail>string</eMail>
//<diaChi>string</diaChi>
//<maTinh>int</maTinh>
//<maHuyen>int</maHuyen>
//<maXaPhuong>int</maXaPhuong>
//<tenDangNhap>string</tenDangNhap>